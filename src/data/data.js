const specialized = [
  {
    id: 1,
    img: "assets/img/logo/nodejs.png",
  },
  {
    id: 2,
    img: "assets/img/logo/react.png",
  },
  // {
  //   id: 3,
  //   img: "assets/img/logo/mongodb.png",
  // },
  {
    id: 3,
    img: "assets/img/logo/flutter-logo-name.png",
  },
  // {
  //   id: 5,
  //   img: "assets/img/logo/next-js-logo.png",
  // },
];

const projects = [
  {
    id: "nodeJsProjects",
    name: "React/NodeJs Projects ",
    color: "#026e00",
    img: "assets/img/logo/js.png",
    description: "E-learning Platform, Express_API_Template, Website, dashboard ",
  },
  {
    id: "Flutter",
    name: "Flutter Projects",
    color: "#027DFD",
    img: "assets/img/logo/flutter-logo.png",
    description: "Mobile Application, Android, IOS",
  },
];

const nodeProjects = [
  {
    id: "1.E-learning",
    name: "E-learning",
    color: "#026e00",
    description:
      "the goal of the platform is to allow inchtechs to make online trainings and to organize talks.",
    client: "Inchtechs",
    date: "June,2020",
    skills: "Design, HTML, CSS, JS, Express JS, MongoDB",
    page: "https://retymee.com/",
  },
  // {
  //   id: "2.suiviapi",
  //   name: "suiviapi",
  //   color: "#026e00",
  //   description:
  //     "suiviapi is an api that I set up to manage the end-of-studies projects of undergraduate and graduate students.",
  //   date: "July,2021",
  //   skills: "JavaScript, Express JS, SQL",
  //   github: "https://gitlab.com/Hacheur-1182/suiviapi",
  // },

  {
    id: "3.express_api_template",
    name: "express_api_template",
    color: "#026e00",
    description:
      "Express API template it's a project that saves me time when I do the backend",
    client: "LeHacheur",
    date: "September,2021",
    skills: "JavaScript, Express JS",
    github: "https://github.com/Hacheur-1182/express_api_template",
  },
  {
    id: "4.openTechnology",
    name: "Open Technology",
    color: "#0064b7",
    description: "website for registration of workshop participants",
    client: "Inchtechs",
    date: "January,2022",
    skills:
        "Design, HTML, CSS, JavaScript, React, Typescript, Express JS, MongoDB",
    page: "https://opentechnology.inchtechs.com/",
    github: "-",
  },{
    id: "2.MyPortfolio",
    name: "My portfolio",
    color: "#0064b7",
    description: "My portfolio",
    date: "January,2022",
    skills: "Design, HTML, CSS, JavaScript, React.",
    page: "https://portfolio.lehacheur.com/",
    github: "-",
  },
];

const reactProject = [
  {
    id: "1.sungu",
    name: "Sungu",
    color: "#042B59",
    description: "Application that lets you know what you're doing with your money",
    client: "Inchtechs",
    date: "January,2022",
    skills:
      "Dart, Kotlin, Swift, FireBase",
    page: "https://play.google.com/store/apps/details?id=cm.lehacheur.sungu",
    github: "-",
  },
  {
    id: "1.shoppy",
    name: "Shoppy MarketSpace",
    color: "#042B59",
    description: "With Shoppy MarketSpace, you can explore, search and browse shops effortlessly and find exactly what you need around you, no matter how rare the product. From fashion to electronics to homeware, our many product categories (over 236) cater for all interests and tastes. Shoppy MarketSpace offers a personalised shopping experience with advanced search options. You can search by image, location, name and features. This gives you a wide range of filtered products that you can compare and negotiate directly with the seller(s). You can also subscribe to their shops to keep up to date with new products, promotions, campaigns, etc.",
    client: "Novobyte LLC",
    date: "January,2022",
    skills:
        "Dart, Kotlin, Swift, FireBase",
    page: "https://play.google.com/store/apps/details?id=com.shoppyit.shoppy",
    github: "-",
  },
  {
    id: "1.blueRecharge",
    name: "Blue Recharge",
    color: "#042B59",
    description: "Official application for buying Camtel credit and activating offers",
    client: "-",
    date: "April,2023",
    skills:
        "Dart, Kotlin, Swift, FireBase",
    page: "https://play.google.com/store/apps/details?id=com.camtel.blueapp.my_blue",
    github: "-",
  },
  // {
  //   id: "2.GesEnergy",
  //   name: "GesEnergy",
  //   color: "#0064b7",
  //   description:
  //     "web application that tracks the energy consumption of a device from Arduino,the application has been converted into an Android application by Cordova ",
  //   date: "August,2021",
  //   skills: "Design, HTML, CSS, JavaScript, React, Express JS, SQL",
  //   page: "http://gesenergy.inchtechs.com/",
  // },
  // {
  //   id: "2.MyPortfolio",
  //   name: "my portfolio",
  //   color: "#0064b7",
  //   description: "My portfolio",
  //   date: "January,2022",
  //   skills: "Design, HTML, CSS, JavaScript, React.",
  //   github: "https://gitlab.com/Hacheur-1182/portfolio",
  // },
];

export { specialized, projects, nodeProjects, reactProject };
