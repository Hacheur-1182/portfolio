import { Link } from "react-router-dom";

const NavBar = () => {
  return (
    <nav
      className="nav nav-navbar nav-dot nav-center  navbar-expand-lg navbar-dark text-white"
      data-navbar={"fixed"}
    >
      <Link className="nav-link " to={"/project"}>
        Work
      </Link>
      <Link className="nav-link" to={"/about"}>
        About
      </Link>
    </nav>
  );
};
export default NavBar;
