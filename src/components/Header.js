import { Link } from "react-router-dom";

const Header = () => {
  return (
    <header
      className="header text-white"
      style={{ backgroundColor: "#080608" }}
    >
      <div className="container">
        <div className="row align-items-center ">
          <div className="col-md-6">
            <br />
            <h1 className="display-5 text-white font-weight-600">
              {"Hi! I'm "}
              <span style={{ backgroundColor: "#544796" }}>{"Dama Yero,"}</span>
              {" Full Stack React, Flutter/NodeJs Based in Cameroon"}
            </h1>
            <br />
            <br />

            <p className={"gap-xy"}>
              <Link
                to={"/project"}
                className="btn btn-outline-light btn-danger letter-spacing-1"
              >
                Portfolio
              </Link>
            </p>

            <br />
            <br />
          </div>
          <div className="col-md-6">
            <div>
              <img
                style={{ backgroundColor: "#080608" }}
                src={"assets/img/profile2.png"}
                alt="profile"
              />
            </div>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
