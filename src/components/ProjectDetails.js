/* eslint-disable jsx-a11y/anchor-is-valid */
import { useParams } from "react-router-dom";
import { nodeProjects, reactProject } from "../data/data";

const ProjectDetails = () => {
  const { projectId } = useParams();

  const filter = [...reactProject, ...nodeProjects].filter((obj) => {
    return obj.id === projectId;
  });

  return (
    <div className="container">
      <div className="row mt-12">
        {filter.map((item) => (
          <div
            key={item.id}
            className="col-lg-12 col-md-4 text-white text-center"
          >
            <h5 className={"font-weight-bold"}>Project detail</h5>

            <p>{item.description}</p>

            <ul className="project-detail mt-7">
              <li>
                {item.client !== undefined ? (
                  <div>
                    <p className={"font-weight-bold"}>Client</p>
                    <span>{item.client}</span>
                  </div>
                ) : (
                  <></>
                )}
              </li>

              <li>
                <p className={"font-weight-bold"}>Date</p>
                <span>{item.date}</span>
              </li>

              <li>
                <p className={" font-weight-bold"}>Skills</p>
                <span>{item.skills}</span>
              </li>

              <li>
                {item.page !== undefined ? (
                  <div>
                    <p className={"font-weight-bold"}>Page</p>
                    <a href={item.page} target={"_blank"} rel="noreferrer">
                      {item.page}
                    </a>
                  </div>
                ) : (
                  <></>
                )}
              </li>
              <li>
                {item.github !== undefined ? (
                  <div>
                    <p className={"font-weight-bold"}>GitHub/GitLab</p>
                    <a href={item.github} target={"_blank"} rel="noreferrer">
                      {item.github}
                    </a>
                  </div>
                ) : (
                  <></>
                )}
              </li>
            </ul>
          </div>
        ))}
      </div>
    </div>
  );
};
export default ProjectDetails;
