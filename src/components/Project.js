import Card from "./Card";

const Project = () => {
  return (
    <div className="container">
      <h1
        className={
          "letter-spacing-2 display-1 font-weight-bold font-title text-right"
        }
      >
        {"< projects >"}
      </h1>

      <Card />
    </div>
  );
};
export default Project;
