import "animate.css";
import Specialized from "./Specialized";

const About = () => {
  return (
    <div id={"about"} className="container">
      <h1
        className={
          "letter-spacing-2 display-1 font-weight-bold mb-5 font-title"
        }
      >
        {"< about Me >"}
      </h1>
      <div className="row ">
        <div className={"col-lg-6 col-sm-6"}>
          <h1 className={"text-white"}>My </h1>
          <h1 className={"text-white"}>Biography </h1>
          <a
            className={
              "btn btn-outline-light mt-5 animate__animated animate__pulse animate__infinite"
            }
            href={"https://1drv.ms/b/s!ApN8Vso5qfLFiSCT1vTM0ZZn-cni?e=8cFl6F"}
            target={"_blank"}
            rel="noreferrer"
          >
            <span className={"iconbox bg-danger mr-2"}>
              <i className="fa fa-arrow-down fa-1x text-white " />
            </span>
            Download CV
          </a>
        </div>
        <div className="col-lg-6 col-sm-6">
          <p className={"text-white mt-7"}>
            {"I'm an innovative, results-oriented developer with over 4 years' experience in " +
                "the design and development of websites, web and mobile applications. " +
                "I am autonomous and able to work efficiently within a team while respecting deadlines. " +
                "I'm also open to feedback and have a constant desire to improve my skills"}
          </p>
        </div>
        <br />
        <br />
      </div>
      <div className="row border border-glass-3 pl-10 mt-9 mb-7 gap-2 gap-y bg-transparent">
        <div className="col-lg-4 col-sm-3 ">
          <p className={"text-white"}>Location</p>
          <span>
            <i className={"fa fa-map-marker fa-2x text-danger mr-2"} />
            Cameroon,Yaounde
          </span>
        </div>
        <div className="col-lg-4 col-sm-3">
          <p className={"text-white"}>Email</p>
          <span>
            <i className={"fa fa-2x fa-envelope-o text-white mr-2"} />
            {"dama.yero.baidi@gmail.com "}
          </span>
        </div>
        <div className="col-lg-4">
          <p className={"text-white"}>Github</p>
          <span>
            <a
              href={"https://github.com/Hacheur-1182"}
              target={"_blank"}
              rel="noreferrer"
            >
              <i className="fa fa-2x fa-github text-white" aria-hidden="true" />
            </a>
          </span>
        </div>
      </div>
      <Specialized />
    </div>
  );
};

export default About;
