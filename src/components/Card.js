import { projects } from "../data/data";
import {Link} from "react-router-dom";

const Card = () => {
  return (
    <div className="row mt-10">
      {projects.map((project) => (
        <div key={project.id} className="col-lg-6 mb-5">
          <div
            className={"card bg-transparent text-white border border-glass-3"}
          >
            <div className="card-body">
              <div className="row">
                <img
                  className={"avatar avatar-sm mr-1"}
                  src={project.img}
                  alt=""
                />
                <p className={"card-text mt-2 "}>{project.name}</p>
              </div>
              <div className="card-text">
                <p className={"ml-6 mt-4"}>{project.description}</p>
              </div>

              <p className={"text-right"}>
                <Link style={{ color: project.color }} to={`/viewproject/${project.id}`}>
                  View projects
                </Link>
              </p>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};
export default Card;
