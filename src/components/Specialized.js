import {specialized} from "../data/data";

const Specialized = () => {
  return (
    <div className="container">
      <div className="row ">
        {specialized.map((item) => (
          <div
            key={item.id}
            data-aos={"zoom"}
            className="col-lg-4 col-sm-4 text-center aos-init aos-animate"
          >
            <img
              className={"animate__animated animate__rollIn animate__delay-1s"}
              src={item.img}
              alt={item.id}
            />
          </div>
        ))}
      </div>
      <h1
        className={
          "letter-spacing-2 display-1 font-weight-bold font-title text-right"
        }
      >
        {"< Specialized >"}
      </h1>
    </div>
  );
};
export default Specialized;
