import Project from "../components/Project";
import About from "../components/About";
import Specialized from "../components/Specialized";

const Body = () => {
  return (
    <section style={{ backgroundColor: "#1a1919" }}>
      <Project />
      <About />
      <Specialized />
    </section>
  );
};
export default Body;
