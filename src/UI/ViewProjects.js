import { Link, useParams } from "react-router-dom";
import { nodeProjects, reactProject } from "../data/data";

const ViewProjects = () => {
  const { projectName } = useParams();
  const projects = { nodeProjects: nodeProjects, reactProject: reactProject };

  return (
    <div className="container ">
      {projectName === "nodeJsProjects" ? (
        <h1 className={"text-white text-center mt-10 mb-8"}>
          Node / React  Projects
        </h1>
      ) : (
        <h1 className={"text-white text-center mt-10 mb-8"}>
          Flutter Projects
        </h1>
      )}

      <div className="row">
        {projectName === "nodeJsProjects"
          ? projects.nodeProjects.map((node) => (
              <div key={node.id} className="col-md-4 mb-4">
                <div
                  style={{ backgroundColor: node.color }}
                  className="card text-white "
                >
                  <div className="card-body">
                    <h5 className="card-title">{node.name}</h5>
                    <p>{node.description.substring(0, 90) + " ..."}</p>
                    <Link
                      className="fs-12 fw-600"
                      to={`/projectDetail/${node.id}`}
                    >
                      Read more <i className="fa fa-angle-right pl-1" />
                    </Link>
                  </div>
                </div>
              </div>
            ))
          : projects.reactProject.map((node) => (
              <div key={node.id} className="col-md-4 mb-4">
                <div style={{backgroundColor: node.color}} className="card text-white ">
                  <div className="card-body">
                    <h5 className="card-title">{node.name}</h5>
                    <p>{node.description.substring(0, 20) + " ..."}</p>
                    <Link
                      className="fs-12 fw-600"
                      to={`/projectDetail/${node.id}`}
                    >
                      Read more <i className="fa fa-angle-right pl-1" />
                    </Link>
                  </div>
                </div>
              </div>
            ))}
      </div>
    </div>
  );
};
export default ViewProjects;
