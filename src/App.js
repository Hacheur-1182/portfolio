import React from "react";
import Home from "./UI/Home";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import Project from "./components/Project";
import About from "./components/About";
import ViewProjects from "./UI/ViewProjects";
import ProjectDetails from "./components/ProjectDetails";
import NotFound from "./UI/NotFound";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path={"/"} element={<Home />} exact />
        <Route path={"/project"} element={<Project />} exact />
        <Route path={"/about"} element={<About />} exact />
        <Route
          path={"/viewproject/:projectName"}
          element={<ViewProjects />}
          exact
        />
        <Route
          path={"/projectDetail/:projectId"}
          element={<ProjectDetails />}
          exact
        />
        <Route path={"*"} exact element={<NotFound />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
